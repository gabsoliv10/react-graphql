import React, { Component }from 'react';
import logo from './../assets/images/logo.jpg';


class Footer extends React.Component {
    render() {
        return (
            <footer className="footer" >
                <div className="container" >
                    <p>© Copyright - React-graphql | by Gabriela Oliveira</p>
                </div>
            </footer>
        );
    }
}

export default Footer;